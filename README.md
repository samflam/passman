Simple interactive CLI based password manager. Use at your own peril.

# Install

Install to with `make install`. You can change the install location with the `PREFIX` variable.

# Usage

Do `passman --help` for a list of subcommands. The first time you run passman you will be asked to create a master password that will be required to access the password database. By default the password database will be encrypted in a file at `~/.passman/accounts`. Each call to passman requires entering the master password. This can get tedious, so there is also an option to run an interactive session. In an interactive session, multiple subcommands can be executed and the master password doesn't need to be reentered for the duration of the session. To start an interactive session do `passman` without any subcommands.

Once the interactive session starts, enter `help` for a list of commands or `quit` to exit. The subcommands available in an interactive session are the same as those available from the command line.
